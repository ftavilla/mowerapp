package com.blablacar.exception;

public class LineInvalidFormatException extends Exception {

    public LineInvalidFormatException() {
        super();
    }

    public LineInvalidFormatException(String message) {
        super(message);
    }
}
