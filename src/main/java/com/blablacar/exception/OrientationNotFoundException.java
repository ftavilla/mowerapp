package com.blablacar.exception;

public class OrientationNotFoundException extends Exception{

    public OrientationNotFoundException(){
        super("Orientation not found!");
    };

    public OrientationNotFoundException(String message) {
        super(message);
    }
}
