package com.blablacar.exception;

public class MowerInstructionNotFoundException extends Exception {

    public MowerInstructionNotFoundException() {
        super("Orientation not found!");
    }

    public MowerInstructionNotFoundException(String message) {
        super(message);
    }
}
