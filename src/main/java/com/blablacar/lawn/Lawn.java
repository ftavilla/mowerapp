package com.blablacar.lawn;

import com.blablacar.mower.Mower;
import com.blablacar.mower.MowerInstruction;
import com.blablacar.position.Position;
import com.blablacar.surface.Surface;
import com.blablacar.utils.transformers.InstructionsLineTransformer;
import com.blablacar.utils.transformers.PositionLineTransformer;
import com.blablacar.utils.transformers.SurfaceLineTransformer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

public class Lawn {

    Surface surface;

    private List<Mower> mowers;

    public Lawn(final String filename) throws Exception {
        buildLawnFromFile(filename);
    }

    public Surface getSurface() {
        return surface;
    }

    public void setSurface(Surface surface) {
        this.surface = surface;
    }

    public List<Mower> getMowers() {
        return mowers;
    }

    public void setMowers(List<Mower> mowers) {
        this.mowers = mowers;
    }

    private void buildLawnFromFile(final String filename) throws Exception {
        BufferedReader reader = new BufferedReader(new FileReader(filename));

        String line;
        Boolean isFirstLine = true;
        List<Mower> mowers = newArrayList();

        while ((line = reader.readLine()) != null) {
            if (isFirstLine) {
                this.surface = new SurfaceLineTransformer(line).transform();
                isFirstLine = false;
            } else {
                mowers.add(buildMower(reader, line));
            }
        }
        reader.close();

        this.mowers = mowers;
    }

    private Mower buildMower(BufferedReader reader, String line) throws Exception {
        Position position = new PositionLineTransformer(line).transform();
        List<MowerInstruction> instructions = new InstructionsLineTransformer(reader.readLine()).transform();
        return new Mower(position, instructions);
    }

    public List<Mower> runMowers() throws Exception {
        for (Mower mower : this.mowers) {
            mower.executeInstructions(this.surface);
        }
        return this.mowers;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Lawn lawn = (Lawn) o;

        if (surface != null ? !surface.equals(lawn.surface) : lawn.surface != null) return false;
        return !(mowers != null ? !mowers.equals(lawn.mowers) : lawn.mowers != null);

    }

    @Override
    public int hashCode() {
        int result = surface != null ? surface.hashCode() : 0;
        result = 31 * result + (mowers != null ? mowers.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Lawn{");
        sb.append("surface=").append(surface);
        sb.append(", mowers=").append(mowers);
        sb.append('}');
        return sb.toString();
    }
}
