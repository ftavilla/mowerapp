package com.blablacar.position;

import com.blablacar.orientation.Orientation;
import com.blablacar.surface.Surface;

public class Position {

    public static final int X_ORIGIN = 0;
    public static final int Y_ORIGIN = 0;

    private Integer x;

    private Integer y;

    private Orientation orientation;

    public Position() {
    }

    public Position(Integer x, Integer y, Orientation orientation) {
        this.x = x;
        this.y = y;
        this.orientation = orientation;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    public void turnLeft() {
        this.orientation = this.orientation.getLeftOrientation();
    }

    public void turnRight() {
        this.orientation = this.orientation.getRightOrientation();
    }

    public void moveForward(Surface surface) {
        switch (this.getOrientation()) {
            case NORTH:
                if (this.y < surface.getySize()) {
                    this.y = this.y + 1;
                }
                break;
            case EAST:
                if (this.x < surface.getxSize()) {
                    this.x = this.x + 1;
                }
                break;
            case SOUTH:
                if (this.y > Y_ORIGIN) {
                    this.y = this.y - 1;
                }
                break;
            case WEST:
                if (this.x > X_ORIGIN) {
                    this.x = this.x - 1;
                }
                break;
        }
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Position{");
        sb.append("x=").append(x);
        sb.append(", y=").append(y);
        sb.append(", orientation=").append(orientation);
        sb.append('}');
        return sb.toString();
    }
}
