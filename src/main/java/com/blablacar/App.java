package com.blablacar;

import com.blablacar.lawn.Lawn;
import com.blablacar.mower.Mower;

import java.util.List;

public class App {

    public static void main(String[] args) throws Exception {
        Lawn lawn = new Lawn(args[0]);
        List<Mower> mowers = lawn.runMowers();

        mowers.stream().forEach((mower) -> {
            StringBuilder result = new StringBuilder()
                    .append(mower.getPosition().getX()).append(" ")
                    .append(mower.getPosition().getY()).append(" ")
                    .append(mower.getPosition().getOrientation().getLabel());
            System.out.println(result);
        });
    }
}