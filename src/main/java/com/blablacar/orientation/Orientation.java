package com.blablacar.orientation;

import com.blablacar.exception.OrientationNotFoundException;

public enum Orientation {

    NORTH("N"), EAST("E"),  SOUTH("S"), WEST("W");

    private String label;

    Orientation(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public static Orientation getOrientationByLabel(String label) throws OrientationNotFoundException {
        for (Orientation orientation : Orientation.values()) {
            if(orientation.getLabel().equals(label)){
                return orientation;
            }
        }
        throw new OrientationNotFoundException(String.format("Orientation with label %s not found!", label));
    }

    public Orientation getRightOrientation() {
        switch (this)
        {
            case NORTH: return EAST;
            case EAST: return SOUTH;
            case SOUTH: return WEST;
            case WEST: return NORTH;
            default: return null;
        }
    }

    public Orientation getLeftOrientation() {
        switch (this)
        {
            case NORTH: return WEST;
            case WEST: return SOUTH;
            case SOUTH: return EAST;
            case EAST: return NORTH;
            default: return null;
        }
    }
}
