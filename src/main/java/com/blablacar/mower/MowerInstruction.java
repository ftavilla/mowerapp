package com.blablacar.mower;

import com.blablacar.exception.MowerInstructionNotFoundException;

public enum MowerInstruction {
    FORWARD("F"), RIGHT("R"), LEFT("L");

    private String label;

    MowerInstruction(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public static MowerInstruction getMowerInstructionByLabel(String label) throws MowerInstructionNotFoundException {
        for (MowerInstruction instruction : MowerInstruction.values()) {
            if (instruction.getLabel().equals(label)) {
                return instruction;
            }
        }
        throw new MowerInstructionNotFoundException(String.format("Mower instruction with label %s not found!", label));
    }


}
