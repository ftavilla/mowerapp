package com.blablacar.mower;

import com.blablacar.position.Position;
import com.blablacar.surface.Surface;

import java.util.List;

import static com.blablacar.mower.MowerInstruction.*;

public class Mower {

    private Position position;

    private List<MowerInstruction> instructions;

    public Mower(Position position, List<MowerInstruction> instructions) {
        this.position = position;
        this.instructions = instructions;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public List<MowerInstruction> getInstructions() {
        return instructions;
    }

    public void setInstructions(List<MowerInstruction> instructions) {
        this.instructions = instructions;
    }

    public void executeInstructions(Surface surface) {
        for (MowerInstruction instruction : this.instructions) {
            doInstruction(surface, instruction);
        }
    }

    private void doInstruction(Surface surface, MowerInstruction instruction) {
        if (FORWARD.equals(instruction)) {
            this.position.moveForward(surface);
        }

        if (RIGHT.equals(instruction)) {
            this.position.turnRight();
        }

        if (LEFT.equals(instruction)) {
            this.position.turnLeft();
        }
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Mower{");
        sb.append("position=").append(position);
        sb.append(", instructions=").append(instructions);
        sb.append('}');
        return sb.toString();
    }
}
