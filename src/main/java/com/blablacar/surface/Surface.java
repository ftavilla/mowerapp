package com.blablacar.surface;

import java.util.Arrays;

public class Surface {

    private Integer xSize;

    private Integer ySize;

    public Surface(Integer xSize, Integer ySize) {
        this.xSize = xSize;
        this.ySize = ySize;
    }

    public Integer getxSize() {
        return xSize;
    }

    public void setxSize(Integer xSize) {
        this.xSize = xSize;
    }

    public Integer getySize() {
        return ySize;
    }

    public void setySize(Integer ySize) {
        this.ySize = ySize;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Surface{");
        sb.append("xSize=").append(xSize);
        sb.append(", ySize=").append(ySize);
        sb.append('}');
        return sb.toString();
    }
}
