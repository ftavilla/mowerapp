package com.blablacar.utils.transformers;

import com.blablacar.exception.LineInvalidFormatException;
import com.blablacar.exception.MowerInstructionNotFoundException;
import com.blablacar.mower.MowerInstruction;

import java.util.List;

import static com.blablacar.mower.MowerInstruction.getMowerInstructionByLabel;
import static com.google.common.collect.Lists.newArrayList;

public class InstructionsLineTransformer extends AbstractLineTransformer<List<MowerInstruction>> {

    public static final String INSTRUCTIONS_LINE_FORMAT = "^[FRL]+$";

    public InstructionsLineTransformer(String line) {
        super(line);
    }

    @Override
    public List<MowerInstruction> transform() throws MowerInstructionNotFoundException, LineInvalidFormatException {
        if (line != null && !line.isEmpty() && line.matches(INSTRUCTIONS_LINE_FORMAT)) {
            String[] splitedInstructions = line.split("");
            List<MowerInstruction> instructions = newArrayList();

            for (String instruction : splitedInstructions) {
                instructions.add(getMowerInstructionByLabel(instruction));
            }
            return instructions;
        }
        throw new LineInvalidFormatException("Line do not have the required format!");
    }
}
