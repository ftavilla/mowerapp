package com.blablacar.utils.transformers;

import com.blablacar.exception.LineInvalidFormatException;
import com.blablacar.exception.MowerInstructionNotFoundException;
import com.blablacar.exception.OrientationNotFoundException;

public abstract class AbstractLineTransformer<T> {

    public static final int X_POSITION_INDEX = 0;
    public static final int Y_POSITION_INDEX = 1;
    public static final int ORIENTATION_INDEX = 2;

    protected final String line;

    protected AbstractLineTransformer(String line) {
        this.line = line;
    }

    public abstract T transform() throws OrientationNotFoundException, MowerInstructionNotFoundException, LineInvalidFormatException;
}
