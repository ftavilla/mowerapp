package com.blablacar.utils.transformers;

import com.blablacar.exception.LineInvalidFormatException;
import com.blablacar.surface.Surface;

public class SurfaceLineTransformer extends AbstractLineTransformer<Surface> {

    public static final String SURFACE_LINE_FORMAT = "^[0-9] [0-9]$";

    public SurfaceLineTransformer(String line) {
        super(line);
    }

    @Override
    public Surface transform() throws LineInvalidFormatException {
        if (line != null && !line.isEmpty() && line.matches(SURFACE_LINE_FORMAT)) {
            String[] splitedLine = line.split(" ");
            int xSize = Integer.parseInt(splitedLine[X_POSITION_INDEX]);
            int ySize = Integer.parseInt(splitedLine[Y_POSITION_INDEX]);
            return new Surface(xSize, ySize);
        }
        throw new LineInvalidFormatException("Line do not have the required format!");
    }
}
