package com.blablacar.utils.transformers;

import com.blablacar.exception.LineInvalidFormatException;
import com.blablacar.exception.OrientationNotFoundException;
import com.blablacar.position.Position;

import static com.blablacar.orientation.Orientation.getOrientationByLabel;

public class PositionLineTransformer extends AbstractLineTransformer<Position> {

    public static final String POSITION_LINE_FORMAT = "^[0-9] [0-9] (N|E|S|W)$";

    public PositionLineTransformer(String line) {
        super(line);
    }

    @Override
    public Position transform() throws OrientationNotFoundException, LineInvalidFormatException {
        if (line != null && !line.isEmpty() && line.matches(POSITION_LINE_FORMAT)) {
            String[] coordinates = line.split(" ");
            Position position = new Position();
            position.setX(Integer.valueOf(coordinates[X_POSITION_INDEX]));
            position.setY(Integer.valueOf(coordinates[Y_POSITION_INDEX]));
            position.setOrientation(getOrientationByLabel(coordinates[ORIENTATION_INDEX]));
            return position;
        }
        throw new LineInvalidFormatException("Line do not have the required format!");
    }
}
