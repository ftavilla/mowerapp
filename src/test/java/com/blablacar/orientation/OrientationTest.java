package com.blablacar.orientation;


import com.blablacar.exception.OrientationNotFoundException;
import org.fest.assertions.Assertions;
import org.hamcrest.CoreMatchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.matchers.JUnitMatchers;
import org.junit.rules.ExpectedException;

import static com.blablacar.orientation.Orientation.*;
import static com.blablacar.orientation.Orientation.getOrientationByLabel;
import static org.fest.assertions.Assertions.assertThat;

public class OrientationTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldReturnNorth() throws Exception {
        String label = "N";

        Orientation orientation = getOrientationByLabel(label);

        assertThat(orientation).isEqualTo(NORTH);
    }

    @Test
    public void shouldThrowOrientationNotFoundException() throws Exception {
        thrown.expect(OrientationNotFoundException.class);
        thrown.expectMessage("Orientation with label P not found!");

        String label = "P";

        getOrientationByLabel(label);
    }
}