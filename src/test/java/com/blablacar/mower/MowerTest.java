package com.blablacar.mower;

import com.blablacar.position.Position;
import com.blablacar.surface.Surface;
import org.junit.Test;

import static com.blablacar.mower.MowerInstruction.*;
import static com.blablacar.orientation.Orientation.*;
import static com.google.common.collect.Lists.newArrayList;
import static org.fest.assertions.Assertions.assertThat;

public class MowerTest {

    @Test
    public void shouldGoForwardAndStayNorthOriented() {
        Surface surface = new Surface(5, 5);
        Mower mower = new Mower(new Position(1, 2, NORTH), newArrayList(FORWARD));

        mower.executeInstructions(surface);

        assertThat(mower.getPosition().getX()).isEqualTo(1);
        assertThat(mower.getPosition().getY()).isEqualTo(3);
        assertThat(mower.getPosition().getOrientation()).isEqualTo(NORTH);
    }

    @Test
    public void shouldStayOnTheSamePositionAndNorthOriented() {
        Surface surface = new Surface(5, 5);
        Mower mower = new Mower(new Position(1, 5, NORTH), newArrayList(FORWARD));

        mower.executeInstructions(surface);

        assertThat(mower.getPosition().getX()).isEqualTo(1);
        assertThat(mower.getPosition().getY()).isEqualTo(5);
        assertThat(mower.getPosition().getOrientation()).isEqualTo(NORTH);
    }

    @Test
    public void shouldGoForwardAndStayEastOriented() {
        Surface surface = new Surface(5, 5);
        Mower mower = new Mower(new Position(1, 2, EAST), newArrayList(FORWARD));

        mower.executeInstructions(surface);

        assertThat(mower.getPosition().getX()).isEqualTo(2);
        assertThat(mower.getPosition().getY()).isEqualTo(2);
        assertThat(mower.getPosition().getOrientation()).isEqualTo(EAST);
    }

    @Test
    public void shouldStayOnTheSamePositionAndEastOriented() {
        Surface surface = new Surface(5, 5);
        Mower mower = new Mower(new Position(5, 5, EAST), newArrayList(FORWARD));

        mower.executeInstructions(surface);

        assertThat(mower.getPosition().getX()).isEqualTo(5);
        assertThat(mower.getPosition().getY()).isEqualTo(5);
        assertThat(mower.getPosition().getOrientation()).isEqualTo(EAST);
    }

    @Test
    public void shouldGoForwardAndStaySouthOriented() {
        Surface surface = new Surface(5, 5);
        Mower mower = new Mower(new Position(1, 2, SOUTH), newArrayList(FORWARD));

        mower.executeInstructions(surface);

        assertThat(mower.getPosition().getX()).isEqualTo(1);
        assertThat(mower.getPosition().getY()).isEqualTo(1);
        assertThat(mower.getPosition().getOrientation()).isEqualTo(SOUTH);
    }

    @Test
    public void shouldStayOnTheSamePositionAndSouthOriented() {
        Surface surface = new Surface(5, 5);
        Mower mower = new Mower(new Position(1, 0, SOUTH), newArrayList(FORWARD));

        mower.executeInstructions(surface);

        assertThat(mower.getPosition().getX()).isEqualTo(1);
        assertThat(mower.getPosition().getY()).isEqualTo(0);
        assertThat(mower.getPosition().getOrientation()).isEqualTo(SOUTH);
    }

    @Test
    public void shouldGoForwardAndStayWestOriented() {
        Surface surface = new Surface(5, 5);
        Mower mower = new Mower(new Position(1, 2, WEST), newArrayList(FORWARD));

        mower.executeInstructions(surface);

        assertThat(mower.getPosition().getX()).isEqualTo(0);
        assertThat(mower.getPosition().getY()).isEqualTo(2);
        assertThat(mower.getPosition().getOrientation()).isEqualTo(WEST);
    }

    @Test
    public void shouldStayOnTheSamePositionAndWestOriented() {
        Surface surface = new Surface(5, 5);
        Mower mower = new Mower(new Position(0, 0, WEST), newArrayList(FORWARD));

        mower.executeInstructions(surface);

        assertThat(mower.getPosition().getX()).isEqualTo(0);
        assertThat(mower.getPosition().getY()).isEqualTo(0);
        assertThat(mower.getPosition().getOrientation()).isEqualTo(WEST);
    }

    @Test
    public void shouldBeOrientedEastAfterTurningRight() {
        Surface surface = new Surface(5, 5);
        Mower mower = new Mower(new Position(0, 0, NORTH), newArrayList(RIGHT));

        mower.executeInstructions(surface);

        assertThat(mower.getPosition().getOrientation()).isEqualTo(EAST);
    }

    @Test
    public void shouldBeOrientedSouthAfterTurningRight() {
        Surface surface = new Surface(5, 5);
        Mower mower = new Mower(new Position(0, 0, EAST), newArrayList(RIGHT));

        mower.executeInstructions(surface);

        assertThat(mower.getPosition().getOrientation()).isEqualTo(SOUTH);
    }

    @Test
    public void shouldBeOrientedWestAfterTurningRight() {
        Surface surface = new Surface(5, 5);
        Mower mower = new Mower(new Position(0, 0, SOUTH), newArrayList(RIGHT));

        mower.executeInstructions(surface);

        assertThat(mower.getPosition().getOrientation()).isEqualTo(WEST);
    }

    @Test
    public void shouldBeOrientedNorthAfterTurningRight() {
        Surface surface = new Surface(5, 5);
        Mower mower = new Mower(new Position(0, 0, WEST), newArrayList(RIGHT));

        mower.executeInstructions(surface);

        assertThat(mower.getPosition().getOrientation()).isEqualTo(NORTH);
    }

    @Test
    public void shouldBeOrientedWestAfterTurningLeft(){
        Surface surface = new Surface(5, 5);
        Mower mower = new Mower(new Position(0, 0, NORTH), newArrayList(LEFT));

        mower.executeInstructions(surface);

        assertThat(mower.getPosition().getOrientation()).isEqualTo(WEST);
    }

    @Test
    public void shouldBeOrientedSouthAfterTurningLeft(){
        Surface surface = new Surface(5, 5);
        Mower mower = new Mower(new Position(0, 0, WEST), newArrayList(LEFT));

        mower.executeInstructions(surface);

        assertThat(mower.getPosition().getOrientation()).isEqualTo(SOUTH);
    }

    @Test
    public void shouldBeOrientedEastAfterTurningLeft(){
        Surface surface = new Surface(5, 5);
        Mower mower = new Mower(new Position(0, 0, SOUTH), newArrayList(LEFT));

        mower.executeInstructions(surface);

        assertThat(mower.getPosition().getOrientation()).isEqualTo(EAST);
    }

    @Test
    public void shouldBeOrientedNorthAfterTurningLeft(){
        Surface surface = new Surface(5, 5);
        Mower mower = new Mower(new Position(0, 0, EAST), newArrayList(LEFT));

        mower.executeInstructions(surface);

        assertThat(mower.getPosition().getOrientation()).isEqualTo(NORTH);
    }

}