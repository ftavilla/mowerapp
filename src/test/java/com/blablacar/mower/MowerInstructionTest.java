package com.blablacar.mower;

import com.blablacar.exception.MowerInstructionNotFoundException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static com.blablacar.mower.MowerInstruction.FORWARD;
import static com.blablacar.mower.MowerInstruction.getMowerInstructionByLabel;
import static org.fest.assertions.Assertions.assertThat;

public class MowerInstructionTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldReturnForward() throws Exception {
        String label = "F";

        MowerInstruction instruction = getMowerInstructionByLabel(label);

        assertThat(instruction).isEqualTo(FORWARD);
    }

    @Test
    public void shouldThrowMowerInstructionNotFoundException() throws Exception {
        thrown.expect(MowerInstructionNotFoundException.class);
        thrown.expectMessage("Mower instruction with label A not found!");

        String label = "A";

        getMowerInstructionByLabel(label);
    }
}