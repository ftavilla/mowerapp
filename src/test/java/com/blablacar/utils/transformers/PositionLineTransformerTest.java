package com.blablacar.utils.transformers;

import com.blablacar.exception.LineInvalidFormatException;
import com.blablacar.position.Position;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static com.blablacar.orientation.Orientation.NORTH;
import static org.fest.assertions.Assertions.assertThat;

public class PositionLineTransformerTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldReturnA13NPosition() throws Exception {
        Position position = new PositionLineTransformer(new String("1 3 N")).transform();

        assertThat(position.getX()).isEqualTo(1);
        assertThat(position.getY()).isEqualTo(3);
        assertThat(position.getOrientation()).isEqualTo(NORTH);
    }

    @Test
    public void shouldThrowLineInvalidFormatExceptionWhenXIsNotANumber() throws Exception {
        thrown.expect(LineInvalidFormatException.class);
        thrown.expectMessage("Line do not have the required format!");

        new PositionLineTransformer(new String("Z 3 N")).transform();
    }

    @Test
    public void shouldThrowLineInvalidFormatExceptionWhenXIsEmpty() throws Exception {
        thrown.expect(LineInvalidFormatException.class);
        thrown.expectMessage("Line do not have the required format!");

        new PositionLineTransformer(new String(" 3 N")).transform();
    }

    @Test
    public void shouldThrowLineInvalidFormatExceptionWhenOrientationIsEmpty() throws Exception {
        thrown.expect(LineInvalidFormatException.class);
        thrown.expectMessage("Line do not have the required format!");

        new PositionLineTransformer(new String("1 3 ")).transform();
    }

    @Test
    public void shouldThrowLineInvalidFormatExceptionWhenOrientationIsA() throws Exception {
        thrown.expect(LineInvalidFormatException.class);
        thrown.expectMessage("Line do not have the required format!");

        new PositionLineTransformer(new String("1 3 A")).transform();
    }
}