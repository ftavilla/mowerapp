package com.blablacar.utils.transformers;

import com.blablacar.exception.LineInvalidFormatException;
import com.blablacar.surface.Surface;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.fest.assertions.Assertions.assertThat;

public class SurfaceLineTransformerTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldReturnA5x5Surface() throws Exception {
        Surface surface = new SurfaceLineTransformer(new String("5 5")).transform();

        assertThat(surface.getxSize()).isEqualTo(5);
        assertThat(surface.getySize()).isEqualTo(5);
    }

    @Test
    public void shouldThrowLineInvalidFormatExceptionWhenXIsNotANumber() throws Exception {
        thrown.expect(LineInvalidFormatException.class);
        thrown.expectMessage("Line do not have the required format!");

        new SurfaceLineTransformer(new String("A 5")).transform();
    }

    @Test
    public void shouldThrowLineInvalidFormatExceptionWhenXIsEmpty() throws Exception {
        thrown.expect(LineInvalidFormatException.class);
        thrown.expectMessage("Line do not have the required format!");

        new SurfaceLineTransformer(new String(" 5")).transform();
    }

    @Test
    public void shouldThrowLineInvalidFormatExceptionWhenThereIs3Value() throws Exception {
        thrown.expect(LineInvalidFormatException.class);
        thrown.expectMessage("Line do not have the required format!");

        new SurfaceLineTransformer(new String("5 5 5")).transform();
    }
}