package com.blablacar.utils.transformers;


import com.blablacar.exception.LineInvalidFormatException;
import com.blablacar.mower.MowerInstruction;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static com.blablacar.mower.MowerInstruction.*;
import static org.fest.assertions.Assertions.assertThat;

public class InstructionsLineTransformerTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldReturnInstructionRLF() throws Exception {
        List<MowerInstruction> instructions = new InstructionsLineTransformer(new String("RLF")).transform();

        assertThat(instructions).containsExactly(RIGHT, LEFT, FORWARD);
    }

    @Test
    public void shouldThrowLineInvalidExceptionWhenLineIsEmpty() throws Exception {
        thrown.expect(LineInvalidFormatException.class);
        thrown.expectMessage("Line do not have the required format!");

        new InstructionsLineTransformer(new String("")).transform();
    }

    @Test
    public void shouldThrowLineInvalidExceptionWhenLineContainsAZECharacter() throws Exception {
        thrown.expect(LineInvalidFormatException.class);
        thrown.expectMessage("Line do not have the required format!");

        new InstructionsLineTransformer(new String("AZE")).transform();
    }
}