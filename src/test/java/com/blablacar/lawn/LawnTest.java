package com.blablacar.lawn;

import org.junit.Test;

import static com.blablacar.orientation.Orientation.EAST;
import static com.blablacar.orientation.Orientation.NORTH;
import static org.fest.assertions.Assertions.assertThat;

public class LawnTest {

    public static final String TEST_FILE_LOCATION = "src/test/java/resources/testFile";

    @Test
    public void shouldReturnPositions13NAnd51E() throws Exception {
        Lawn lawn = new Lawn(TEST_FILE_LOCATION);

        lawn.runMowers();

        assertThat(lawn.getMowers()).onProperty("position").onProperty("x").containsExactly(1, 5);
        assertThat(lawn.getMowers()).onProperty("position").onProperty("y").containsExactly(3, 1);
        assertThat(lawn.getMowers()).onProperty("position").onProperty("orientation").containsExactly(NORTH, EAST);
    }

    @Test
    public void shouldBuildALawnWithA5x5Surface() throws Exception {
        Lawn lawn = new Lawn(TEST_FILE_LOCATION);

        assertThat(lawn).isNotNull();
        assertThat(lawn.getSurface().getxSize()).isEqualTo(5);
        assertThat(lawn.getSurface().getySize()).isEqualTo(5);
    }

    @Test
    public void shouldBuildLawnWithTwoMowers() throws Exception {
        Lawn lawn = new Lawn(TEST_FILE_LOCATION);

        assertThat(lawn).isNotNull();
        assertThat(lawn.getMowers()).hasSize(2);
        assertThat(lawn.getMowers()).onProperty("position").onProperty("x").containsExactly(1, 3);
        assertThat(lawn.getMowers()).onProperty("position").onProperty("y").containsExactly(2, 3);
        assertThat(lawn.getMowers()).onProperty("position").onProperty("orientation").containsExactly(NORTH, EAST);
    }

}